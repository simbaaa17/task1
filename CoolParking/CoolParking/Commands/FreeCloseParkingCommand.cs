﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class FreeCloseParkingCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public FreeCloseParkingCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            var openPlaceCount = _parkingService.GetFreePlaces();
            var closePlaceCount = _parkingService.GetCapacity() - openPlaceCount;

            Console.WriteLine($"Free places: {openPlaceCount}, Close places: {closePlaceCount}");
        }
    }
}
