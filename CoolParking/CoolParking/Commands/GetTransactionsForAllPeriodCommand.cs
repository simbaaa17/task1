﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class GetTransactionsForAllPeriodCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public GetTransactionsForAllPeriodCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            Console.WriteLine(_parkingService.ReadFromLog());

        }
    }
}
