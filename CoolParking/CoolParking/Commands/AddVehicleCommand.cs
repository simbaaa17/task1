﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class AddVehicleCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public AddVehicleCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            if (_parkingService.GetFreePlaces() == 0)
            {
                Console.WriteLine("No free places");
                return;
            }

            string vehicleId = string.Empty;
            do
            {
                vehicleId = InputValidator.InputStringValue("Enter vehicleId:");
            }
            while (!VehicleValidator.IsIdValid(vehicleId));

            decimal balance;
            do
            {
                balance = InputValidator.InputDecimalValue("Enter balance:");
            }
            while (!VehicleValidator.IsBalanceValid(balance));

            VehicleType vehicleType;
            string type = string.Empty;
            do
            {
                type = InputValidator.InputStringValue("Enter VehicleType:");
            }
            while (!VehicleValidator.IsVehicleTypeValid(type));

            vehicleType = (VehicleType)Enum.Parse(typeof(VehicleType), type);
            

            var vehicle = new Vehicle(vehicleId, vehicleType, balance);
            _parkingService.AddVehicle(vehicle);
        }
    }
}
//string vehicleId = string.Empty;
//do
//{
//    Console.WriteLine("Enter vehicleId: ");
//    string id = Console.ReadLine();
//    if (VehicleValidator.IsIdValid(id))
//    {
//        vehicleId = id;
//        break;
//    }
//    else
//        Console.WriteLine("Wrong input vehicle Id");
//}
//while (true);
//decimal balance;
//do
//{
//    Console.WriteLine("Enter balance:");
//    if (decimal.TryParse(Console.ReadLine(), out balance) && VehicleValidator.IsBalanceValid(balance))
//        break;
//    else
//        Console.WriteLine("Wrong input balance");
//}
//while (true);
//VehicleType vehicleType;
//do
//{
//    Console.WriteLine("Enter VehicleType:");

//    string type = Console.ReadLine();
//    if (VehicleValidator.IsVehicleTypeValid(type))
//    { 
//        vehicleType = (VehicleType)Enum.Parse(typeof(VehicleType), type);
//        break;
//    }
//    else
//        Console.WriteLine("Wrong input vehicle Id");
//}
//while (true);