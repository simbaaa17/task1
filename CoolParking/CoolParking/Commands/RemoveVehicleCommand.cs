﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class RemoveVehicleCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public RemoveVehicleCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {

            string vehicleId = string.Empty;
            do
            {
                vehicleId = InputValidator.InputStringValue("Enter vehicleId:");
            }
            while (!VehicleValidator.IsIdValid(vehicleId));

            _parkingService.RemoveVehicle(vehicleId);
        }
    }
}
