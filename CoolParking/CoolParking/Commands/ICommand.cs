﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking
{
    public interface ICommand
    {
        void Execute();
    }
}
