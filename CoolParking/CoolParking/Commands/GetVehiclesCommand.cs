﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class GetVehiclesCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public GetVehiclesCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            var sb = new StringBuilder();

            foreach (var vehicle in _parkingService.GetVehicles())
            {
                sb.Append(vehicle);
            }

            Console.WriteLine(sb);
        }
    }
}
