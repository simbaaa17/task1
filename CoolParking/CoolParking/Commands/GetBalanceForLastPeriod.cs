﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.Commands
{
    public class GetBalanceForLastPeriodCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public GetBalanceForLastPeriodCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            Console.WriteLine($"Current balance of parking for last period: {_parkingService.GetLastParkingTransactions().Sum(t=>t.Sum)}");
        }
    }
}
