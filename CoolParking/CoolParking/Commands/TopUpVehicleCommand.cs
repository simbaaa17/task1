﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class TopUpVehicleCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public TopUpVehicleCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            string vehicleId = string.Empty;
            do
            {
                vehicleId = InputValidator.InputStringValue("Enter vehicleId:");
            }
            while (!VehicleValidator.IsIdValid(vehicleId));


            decimal balance;
            do
            {
                balance = InputValidator.InputDecimalValue("Enter balance:");
            }
            while (!VehicleValidator.IsBalanceValid(balance));

            _parkingService.TopUpVehicle(vehicleId, balance);

            Console.WriteLine("Successfully top uped balance");
        }
    }
}
