﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class GetTransactionsForLastPeriodCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public GetTransactionsForLastPeriodCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            var sb = new StringBuilder();

            var transactions = _parkingService.GetLastParkingTransactions();

            foreach (var transaction in transactions)
            {
                sb.Append(transaction);
            }

            Console.WriteLine(sb);

        }
    }
}
