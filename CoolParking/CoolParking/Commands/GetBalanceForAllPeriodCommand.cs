﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Commands
{
    public class GetBalanceForAllPeriodCommand : ICommand
    {
        private readonly IParkingService _parkingService;

        public GetBalanceForAllPeriodCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            Console.WriteLine($"Current balance of parking for all period: {_parkingService.GetBalance()}");
        }
    }
}
