﻿using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.Commands;
using CoolParking.BL.Interfaces;

namespace CoolParking
{
   
    public class ConsoleMenu : IMenu
    {
        public Dictionary<int, ICommand> Commands { get; set; } = new Dictionary<int, ICommand>();

        private readonly IParkingService _parkingService;
        public ConsoleMenu(IParkingService parkingService)
        {
            _parkingService = parkingService;

            Commands.Add(1, new GetBalanceForAllPeriodCommand(_parkingService));
            Commands.Add(2, new GetBalanceForLastPeriodCommand(_parkingService));
            Commands.Add(3, new FreeCloseParkingCommand(_parkingService));
            Commands.Add(4, new GetTransactionsForLastPeriodCommand(_parkingService));
            Commands.Add(5, new GetTransactionsForAllPeriodCommand(_parkingService));
            Commands.Add(6, new GetVehiclesCommand(_parkingService));
            Commands.Add(7, new AddVehicleCommand(_parkingService));
            Commands.Add(8, new RemoveVehicleCommand(_parkingService));
            Commands.Add(9, new TopUpVehicleCommand(_parkingService));
            Commands.Add(10, new ExitCommand());

        }

        public void Show()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. Balance for all period");
            Console.WriteLine("2. Balance for last period");
            Console.WriteLine("3. Free/Occupied place");
            Console.WriteLine("4. Transactions for last period");
            Console.WriteLine("5. Transactions for all period");
            Console.WriteLine("6. All vehicles");
            Console.WriteLine("7. Add vehicle");
            Console.WriteLine("8. Remove vehicle");
            Console.WriteLine("9. Topup vehicle");
            Console.WriteLine("10. Exit");
        }

        public void Run()
        {
            int choose;
            ICommand currentCommand = null;

            do
            {
                Show();
               
                if (int.TryParse(Console.ReadLine(), out choose))
                {
                    if (Commands.TryGetValue(choose, out currentCommand))
                    {
                        currentCommand.Execute();
                    }
                    else
                    {
                        Console.WriteLine("Repeat your choose"); 
                    }
                }
            }
            while (!(currentCommand is ExitCommand));
        }
     
    }
}
