﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking
{
    public class InputValidator
    {
        public static string InputStringValue(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        public static decimal InputDecimalValue(string message)
        {
            decimal value;
            do
            {
                Console.WriteLine(message);
                if (decimal.TryParse(Console.ReadLine(), out value))
                    return value;
                else
                    Console.WriteLine("Wrong input value");
            }
            while (true);
        }  
    }
}