﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CoolParking.Extensions;

namespace CoolParking
{

    class Program
    {
        static HttpClient client = new HttpClient();
        static async Task<string> GetBalanceAsync()
        {
            HttpResponseMessage response = await client.GetAsync("api/parking/balance");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> GetCapacityAsync()
        {
            HttpResponseMessage response = await client.GetAsync("api/parking/capacity");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> GetFreePlacesAsync()
        {
            HttpResponseMessage response = await client.GetAsync("api/parking/freePlaces");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> GetVehiclesAsync()
        {
            HttpResponseMessage response = await client.GetAsync("api/vehicles");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> GetVehicleByIdAsync(string vehicleId)
        {
            HttpResponseMessage response = await client.GetAsync($"api/vehicles/{vehicleId}");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> AddVehicleAsync(Vehicle vehicle)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync("api/vehicles", vehicle);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<HttpStatusCode> DeleteVehicleAsync(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync($"api/vehicles/{id}");
            response.EnsureSuccessStatusCode();

            return response.StatusCode;
        }

        static async Task<string> GetLastTransactionsAsync()
        {
            HttpResponseMessage response = await client.GetAsync($"api/transactions/last");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> GetAllTransactionsAsync()
        {
            HttpResponseMessage response = await client.GetAsync($"api/transactions/all");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task<string> TopUpVehicleAsync(string vehicleId, decimal sum)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync($"api/vehicles/topUpVehicle", new { Id = vehicleId, Sum = sum });
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        static async Task Main(string[] args)
        {
            await RunAsync();
        }

        static async Task RunAsync()
        {
            client.BaseAddress = new Uri("http://localhost:5000/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {

                var vehicle1 = new Vehicle("AA-0001-AA", VehicleType.Truck, 100);
                var vehicle2 = new Vehicle("AA-0002-AA", VehicleType.Bus, 100);

                var vehicle = await AddVehicleAsync(vehicle1);
                Console.WriteLine($"Vehicle: {vehicle}");

                vehicle = await AddVehicleAsync(vehicle2);
                Console.WriteLine($"Vehicle: {vehicle}");

                vehicle = await TopUpVehicleAsync(vehicle2.Id, 100);
                Console.WriteLine($"Vehicle: {vehicle}");

                vehicle = await GetVehicleByIdAsync(vehicle2.Id);
                Console.WriteLine($"Vehicle: {vehicle}");

                var balance = await GetBalanceAsync();
                Console.WriteLine($"Balance {balance}");

                var capacity = await GetCapacityAsync();
                Console.WriteLine($"Capacity {capacity}");

                var freePlaces = await GetFreePlacesAsync();
                Console.WriteLine($"FreePlaces {freePlaces}");

                var vehicles = await GetVehiclesAsync();
                Console.WriteLine($"Vehicles {vehicles}");

                var statusCode  = await DeleteVehicleAsync(vehicle1.Id);
                Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");

                vehicles = await GetVehiclesAsync();
                Console.WriteLine($"Vehicles {vehicles}");

                var transactions = await GetLastTransactionsAsync();
                Console.WriteLine($"Last Transactions {transactions}");

                transactions = await GetAllTransactionsAsync();
                Console.WriteLine($"All Transactions {transactions}");

                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
