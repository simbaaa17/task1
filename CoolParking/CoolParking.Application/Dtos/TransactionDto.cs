﻿using System;
using System.Runtime.Serialization;


namespace CoolParking.Application.Dtos
{
    [DataContract]
    public class TransactionDto
    {
        [DataMember]
        public string VehicleId { get; set; }

        [DataMember]
        public DateTime TransactionDate { get; set; }

        [DataMember]
        public decimal Sum { get; set; }

    }
}
