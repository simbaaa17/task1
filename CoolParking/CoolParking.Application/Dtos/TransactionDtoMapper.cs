﻿using AutoMapper;

namespace CoolParking.Application.Dtos
{
    class TransactionDtoMapper : Profile
    {
        public TransactionDtoMapper()
        {
            CreateMap<TransactionInfo, TransactionDto>()
                .ForMember(d => d.TransactionDate, o => o.MapFrom(s => s.CreationDate));
        }
    }
}
