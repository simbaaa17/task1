﻿using AutoMapper;
using CoolParking.Application.Commands.AddVehicle;

namespace CoolParking.Application.Dtos
{
    public class VehicleDtoMapper : Profile
    {
        public VehicleDtoMapper()
        {
            CreateMap<AddVehicleCommand, VehicleDto>();
            CreateMap<Vehicle, VehicleDto>();
        }
    }
}
