﻿using System.Runtime.Serialization;

namespace CoolParking.Application.Dtos
{
    [DataContract]
    public class VehicleDto
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public VehicleType VehicleType { get; set; }

        [DataMember]
        public decimal Balance { get; set; }

    }
}
