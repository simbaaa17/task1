﻿using MediatR;
using System.Runtime.Serialization;
using CoolParking.Application.Dtos;
using System.Collections.Generic;

namespace CoolParking.Application.Queries.Vehicles
{
    [DataContract]
    public class VehiclesQuery : IRequest<IEnumerable<VehicleDto>>
    {
    }
}
