﻿using System;

namespace CoolParking.Application.Queries.Vehicles
{
    public class NotFoundVehicleException : Exception
    {
        public NotFoundVehicleException() { }
        public NotFoundVehicleException(string message) : base(message) { }
        public NotFoundVehicleException(string message, Exception inner) : base(message, inner) { }
    }
}
