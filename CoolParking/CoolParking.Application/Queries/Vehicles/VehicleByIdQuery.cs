﻿using CoolParking.Application.Dtos;
using MediatR;
using System.Runtime.Serialization;

namespace CoolParking.Application.Queries.Vehicles
{
    [DataContract]
    public class VehicleByIdQuery : IRequest<VehicleDto>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
