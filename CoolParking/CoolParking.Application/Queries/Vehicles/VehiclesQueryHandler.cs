﻿using AutoMapper;
using CoolParking.Application.Dtos;
using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.Vehicles
{
    public class VehiclesQueryHandler: IRequestHandler<VehiclesQuery, IEnumerable<VehicleDto>>
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public VehiclesQueryHandler(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<IEnumerable<VehicleDto>> Handle(VehiclesQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var vehicles = _mapper.Map<IEnumerable<VehicleDto>>(_parkingService.GetVehicles());

            return Task.FromResult(vehicles);
        }
    }
}
