﻿using AutoMapper;
using CoolParking.Application.Dtos;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.Vehicles
{
    public class VehicleByIdQueryHandler : IRequestHandler<VehicleByIdQuery, VehicleDto>
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public VehicleByIdQueryHandler(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<VehicleDto> Handle(VehicleByIdQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            if (!VehicleValidator.IsIdValid(request.Id))
                throw new VehicleIdValidationPolicyViolationException("Invalid identifier");

            var vehicle = _parkingService.GetVehicles()
                .SingleOrDefault(v => v.Id == request.Id);

            if (vehicle == null)
                throw new NotFoundVehicleException($"Not found vehicle with id = {request.Id}");
            
            return Task.FromResult(_mapper.Map<VehicleDto>(vehicle));
        }
    }
}
