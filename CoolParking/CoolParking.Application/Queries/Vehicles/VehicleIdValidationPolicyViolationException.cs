﻿using System;

namespace CoolParking.Application.Queries.Vehicles
{

    public class VehicleIdValidationPolicyViolationException : Exception
    {
        public VehicleIdValidationPolicyViolationException() { }
        public VehicleIdValidationPolicyViolationException(string message) : base(message) { }
        public VehicleIdValidationPolicyViolationException(string message, Exception inner) : base(message, inner) { }
       
    }
}
