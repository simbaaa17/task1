﻿using MediatR;
using System.Runtime.Serialization;

namespace CoolParking.Application.Queries.Balances
{
    [DataContract]
    public class BalanceQuery : IRequest<decimal>
    {
    }
}
