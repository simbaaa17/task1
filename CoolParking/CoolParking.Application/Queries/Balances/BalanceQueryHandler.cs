﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.Balances
{
    public class BalanceQueryHandler : IRequestHandler<BalanceQuery, decimal>
    {
        private readonly IParkingService _parkingService;

        public BalanceQueryHandler(IParkingService parkingService)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
        }
        public Task<decimal> Handle(BalanceQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            return Task.FromResult(_parkingService.GetBalance());
        }
    }
}
