﻿using CoolParking.Application.Dtos;
using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CoolParking.Application.Queries.Transactions
{
    [DataContract]
    public class TransactionsLastQuery : IRequest<IEnumerable<TransactionDto>>
    {
    }
}
