﻿using AutoMapper;
using CoolParking.Application.Dtos;
using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.Transactions
{
    public class TransactionsLastQueryHandler : IRequestHandler<TransactionsLastQuery, IEnumerable<TransactionDto>>
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public TransactionsLastQueryHandler(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<IEnumerable<TransactionDto>> Handle(TransactionsLastQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var transactions = _mapper.Map<IEnumerable<TransactionDto>>(_parkingService.GetLastParkingTransactions());

            return Task.FromResult(transactions);
        }
    }
}
