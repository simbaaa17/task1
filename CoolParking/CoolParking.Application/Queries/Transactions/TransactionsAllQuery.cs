﻿using MediatR;
using System.Runtime.Serialization;

namespace CoolParking.Application.Queries.Transactions
{
    [DataContract]
    public class TransactionsAllQuery : IRequest<string>
    {
    }
}
