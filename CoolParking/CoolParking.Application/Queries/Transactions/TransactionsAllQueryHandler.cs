﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.Transactions
{
    public class TransactionsAllQueryHandler : IRequestHandler<TransactionsAllQuery, string>
    {
        private readonly IParkingService _parkingService;

        public TransactionsAllQueryHandler(IParkingService parkingService)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));

        }
        public Task<string> Handle(TransactionsAllQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            return Task.FromResult(_parkingService.ReadFromLog());
        }
    }
}