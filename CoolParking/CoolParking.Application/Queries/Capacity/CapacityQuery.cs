﻿using MediatR;
using System.Runtime.Serialization;

namespace CoolParking.Application.Queries.Capacity
{
    [DataContract]
    public class CapacityQuery : IRequest<int>
    {
    }
}
