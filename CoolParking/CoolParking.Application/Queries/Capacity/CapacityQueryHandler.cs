﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.Capacity
{
    public class CapacityQueryHandler : IRequestHandler<CapacityQuery, int>
    {
        private readonly IParkingService _parkingService;

        public CapacityQueryHandler(IParkingService parkingService)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
        }
        public Task<int> Handle(CapacityQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            return Task.FromResult(_parkingService.GetCapacity());
        }
    }
}
