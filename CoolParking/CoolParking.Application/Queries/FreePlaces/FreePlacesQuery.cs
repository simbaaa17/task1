﻿using MediatR;
using System.Runtime.Serialization;

namespace CoolParking.Application.Queries.FreePlaces
{
    [DataContract]
    public class FreePlacesQuery : IRequest<int>
    {
    }
}
