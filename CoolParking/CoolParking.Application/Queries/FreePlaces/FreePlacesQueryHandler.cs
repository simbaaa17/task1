﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Queries.FreePlaces
{
    public class FreePlacesQueryHandler : IRequestHandler<FreePlacesQuery, int>
    {
        private readonly IParkingService _parkingService;

        public FreePlacesQueryHandler(IParkingService parkingService)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
        }
        public Task<int> Handle(FreePlacesQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            return Task.FromResult(_parkingService.GetFreePlaces());
        }
    }
}
