﻿using CoolParking.Application.Dtos;
using MediatR;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace CoolParking.Application.Commands.AddVehicle
{
    [DataContract]
    public class AddVehicleCommand : IRequest<VehicleDto>
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public VehicleType VehicleType { get; set; }

        [DataMember]
        public decimal Balance { get; set; }
    }
}
