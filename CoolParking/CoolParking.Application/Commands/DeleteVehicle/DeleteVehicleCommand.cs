﻿using MediatR;
using System.Runtime.Serialization;

namespace CoolParking.Application.Commands.DeleteVehicle
{
    [DataContract]
    public class DeleteVehicleCommand : IRequest
    {
        [DataMember]
        public string Id { get; set; }
    }
}
