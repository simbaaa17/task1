﻿using AutoMapper;
using CoolParking.Application.Queries.Vehicles;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Commands.DeleteVehicle
{
    class DeleteVehicleCommandHandler : IRequestHandler<DeleteVehicleCommand>
    {
        private readonly IParkingService _parkingService;

        public DeleteVehicleCommandHandler(IParkingService parkingService)
        {
            _parkingService = parkingService ?? throw new ArgumentNullException(nameof(parkingService));
        }
        
        public Task<Unit> Handle(DeleteVehicleCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            if(!VehicleValidator.IsIdValid(request.Id))
                throw new VehicleIdValidationPolicyViolationException("Invalid identifier");

            _parkingService.RemoveVehicle(request.Id);

            return Task.FromResult(Unit.Value);
        }
    }
}
