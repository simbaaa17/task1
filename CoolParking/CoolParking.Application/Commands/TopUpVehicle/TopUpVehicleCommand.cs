﻿using CoolParking.Application.Dtos;
using MediatR;
using Newtonsoft.Json;
using System.Runtime.Serialization;


namespace CoolParking.Application.Commands.TopUpVehicle
{
    [DataContract]
    public class TopUpVehicleCommand : IRequest<VehicleDto>
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public decimal Sum { get; set; }
    }
}
