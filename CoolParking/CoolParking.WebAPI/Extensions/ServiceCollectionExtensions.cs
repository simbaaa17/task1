﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CoolParking.WebAPI.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddParkingService(this IServiceCollection services)
        {
            string path = @"C:\Users\Алёша\Documents\GitHub\bsa20-dotnet-hw2-template\CoolParking\CoolParking\bin\Debug\netcoreapp3.1";

            services.AddSingleton<IParkingService, ParkingService>(service =>
            {
                var logService = new LogService(path + "\\Transactions.log");

                var logTimer = new TimerService(Settings.logTime * 1000);
                var withdrawTimer = new TimerService(Settings.withdrawTime * 1000);

                return new ParkingService(withdrawTimer, logTimer, logService);
            });

        }
    }
}
