﻿using CoolParking.WebAPI.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace CoolParking.WebAPI.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseApplicationMiddlewares(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}
