﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    public abstract class MediatorController : Controller
    {
        protected readonly IMediator Mediator;

        public MediatorController(IMediator mediator)
        {
            Mediator = mediator;
        }

        protected async Task<IActionResult> ExecuteQuery<T>(IRequest<T> query)
        {
            if (query == null)
                return BadRequest();

            var entity = await Mediator.Send(query);

            if (entity == null)
                return NotFound();

            return Json(entity);
        }
    }
}
