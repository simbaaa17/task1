﻿using CoolParking.Application.Commands.AddVehicle;
using CoolParking.Application.Commands.DeleteVehicle;
using CoolParking.Application.Commands.TopUpVehicle;
using CoolParking.Application.Dtos;
using CoolParking.Application.Queries.Vehicles;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class VehiclesController : MediatorController
    {
        public VehiclesController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<VehicleDto>))]
        public Task<IActionResult> GetVehicles(VehiclesQuery query) => ExecuteQuery(query);

        [HttpGet("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(VehicleDto))]
        public Task<IActionResult> GetVehicleById(VehicleByIdQuery query) => ExecuteQuery(query);

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [Produces(typeof(VehicleDto))]
        public async Task<IActionResult> AddVehicle([FromBody] AddVehicleCommand command)
        {
            if (command is null)
                return BadRequest();

            var vehicle = await Mediator.Send(command);

            return CreatedAtAction("AddVehicle", new { id = vehicle.Id }, vehicle);
        }

        [HttpDelete("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> DeleteVehicle(DeleteVehicleCommand command)
        {
            if (command is null)
                return BadRequest();

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPut("topUpVehicle")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(VehicleDto))]
        public async Task<IActionResult> TopUpVehicle([FromBody] TopUpVehicleCommand command)
        {
            if (command is null)
                return BadRequest();

            return Json(await Mediator.Send(command));
        }

    }
}
