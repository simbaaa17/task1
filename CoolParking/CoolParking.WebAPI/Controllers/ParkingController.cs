﻿using CoolParking.Application.Queries.Balances;
using CoolParking.Application.Queries.Capacity;
using CoolParking.Application.Queries.FreePlaces;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class ParkingController : MediatorController
    {
        public ParkingController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("balance")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(decimal))]
        public Task<IActionResult> GetBalance(BalanceQuery query) => ExecuteQuery(query);

        [HttpGet("capacity")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(int))]
        public Task<IActionResult> GetCapacity(CapacityQuery query) => ExecuteQuery(query);

        [HttpGet("freePlaces")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(int))]
        public Task<IActionResult> GetFreePlaces(FreePlacesQuery query) => ExecuteQuery(query);

    }
}
