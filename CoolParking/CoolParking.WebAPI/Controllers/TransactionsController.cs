﻿using CoolParking.Application.Dtos;
using CoolParking.Application.Queries.Transactions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class TransactionsController : MediatorController
    {
        public TransactionsController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("last")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<TransactionDto>))]
        public Task<IActionResult> GetLastVehicles(TransactionsLastQuery query) => ExecuteQuery(query);

        [HttpGet("all")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<TransactionDto>))]
        public Task<IActionResult> GetAllVehicles(TransactionsAllQuery query) => ExecuteQuery(query);

    }
}
