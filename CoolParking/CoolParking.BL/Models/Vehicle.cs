﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using CoolParking.BL.Models;
using System;
using System.Text.RegularExpressions;

    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = !string.IsNullOrWhiteSpace(id) && VehicleValidator.IsIdValid(id)
                ? id
                : throw new ArgumentException(nameof(id));

            Balance = balance >= 0
                ? balance
                : throw new ArgumentException(nameof(balance));

            VehicleType = vehicleType;
        }

        public void ChangeBalance(decimal sum)
        {
            Balance += sum;
        }
        public override bool Equals(object obj)
        {
            return obj is Vehicle other
                && GetType() == other.GetType()
                && (Id.Equals(other.Id) || object.ReferenceEquals(this, other));
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return $"Id: {Id}, VehicleType: {VehicleType}, Balance: {Balance}\n";
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            string vehicleId = string.Empty;

            Random rnd = new Random();

            for (int i = 0; i < 2; i++)
            {
                int letter_num = rnd.Next(0, letters.Length - 1);
                vehicleId += letters[letter_num];
            }

            vehicleId += '-';

            for (int i = 0; i < 4; i++)
            {
                vehicleId += rnd.Next(0, 9);
            }
            vehicleId += '-';

            for (int i = 0; i < 2; i++)
            {
                int letter_num = rnd.Next(0, letters.Length - 1);
                vehicleId += letters[letter_num];
            }

            return vehicleId;
        }
    }
