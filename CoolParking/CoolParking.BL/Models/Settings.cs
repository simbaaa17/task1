﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;



    public static class Settings
    {
        public static decimal balance = 0;
        public static int capacity = 10;
        public static int withdrawTime = 5;
        public static int logTime = 60;
        public static decimal ratio = 2.5M;

        public static Dictionary<VehicleType, decimal> rates;

        static Settings()
        {
            rates = new Dictionary<VehicleType, decimal>
            {
                [VehicleType.Bus] = 3.5M,
                [VehicleType.Motorcycle] = 1,
                [VehicleType.PassengerCar] = 2,
                [VehicleType.Truck] = 5
            };
        }
    }
