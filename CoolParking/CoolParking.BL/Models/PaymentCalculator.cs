﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public static class PaymentCalculator
    {
        public static decimal Calculate(decimal vehicleBalance, decimal rate)
        {
            if (vehicleBalance < 0)
                return rate * Settings.ratio;
            else if (rate > vehicleBalance)
            {
                var diff = vehicleBalance - rate;
                return Math.Abs(diff) * Settings.ratio + vehicleBalance;
            }
            else
                return rate;
        }
    }
}
