﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

public class Parking
{
    private static readonly Lazy<Parking> lazy =
       new Lazy<Parking>(() => new Parking());

    public decimal Balance { get; set; }
    public int Capacity { get; set; }
    public List<Vehicle> Vehicles { get; set; }

    private Parking()
    {
        Vehicles = new List<Vehicle>();
        Capacity = Settings.capacity;
        Balance = Settings.balance;
    }

    public void Reset()
    {
        Vehicles = new List<Vehicle>();
        Capacity = Settings.capacity;
        Balance = Settings.balance;
    }

    public void ChangeBalance(decimal sum)
    {
        Balance += sum;
    }
    public static Parking GetInstance()
    {
        return lazy.Value;
    }
}
