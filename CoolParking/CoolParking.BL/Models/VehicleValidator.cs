﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class VehicleValidator
    {

        private static readonly Regex ValidationRegex = new Regex( @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

        public static bool IsIdValid(string id)
        {
            if (ValidationRegex.IsMatch(id))
                return true;
            else
                Console.WriteLine("'{0}' is not a valid of the VehicleId.", id);

            return false;
        }

        public static bool IsBalanceValid(decimal balance)
        {
            if (balance > 0)
                return true;
            else
                Console.WriteLine("'{0}' is not a valid of the balance.", balance);
            
            return false;
        }

        public static bool IsVehicleTypeValid(string vehicleType)
        {
            try
            {
                var vehicle = Enum.Parse(typeof(VehicleType), vehicleType);
                return true;
      
            }
            catch (ArgumentException)
            {
                Console.WriteLine("'{0}' is not a member of the VehicleType enumeration.", vehicleType);
            }

            return false;
        }
    }
}
