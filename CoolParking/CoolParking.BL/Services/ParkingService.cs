﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly Parking _parking = Parking.GetInstance();
        public List<TransactionInfo> TransactionInfos { get; set; } = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer ?? throw new ArgumentNullException(nameof(withdrawTimer));
            _logTimer = logTimer ?? throw new ArgumentNullException(nameof(logTimer));
            _logService = logService ?? throw new ArgumentNullException(nameof(logService));

            _withdrawTimer.Elapsed += WithdrawEvent;
            _withdrawTimer.Start();

            _logTimer.Elapsed += LogEvent;
            _logTimer.Start();
        }

        public void WithdrawEvent(object sender, ElapsedEventArgs e)
        {
            var currentDate = DateTime.Now; 

            foreach (var vehicle in _parking.Vehicles)
            {
                if(Settings.rates.TryGetValue(vehicle.VehicleType, out decimal rate))
                {
                    decimal paymentSum = PaymentCalculator.Calculate(vehicle.Balance, rate);

                    vehicle.ChangeBalance(-paymentSum);
                    _parking.ChangeBalance(paymentSum);

                    var transactionInfo = new TransactionInfo
                    {
                        VehicleId = vehicle.Id,
                        Sum = paymentSum,
                        CreationDate = currentDate
                    };

                    TransactionInfos.Add(transactionInfo);
                }
                else
                {
                    throw new NotSupportedException(nameof(vehicle.VehicleType));
                }
            }
        }

        public void LogEvent(object sender, ElapsedEventArgs e)
        {
            var sb = new StringBuilder();

            foreach(var transaction in TransactionInfos)
            {
                sb.Append(transaction);
            }

            _logService.Write(sb.ToString());

            TransactionInfos.Clear();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _ = vehicle ?? throw new ArgumentNullException(nameof(vehicle));

            if (_parking.Vehicles.Contains(vehicle))
                throw new ArgumentException(nameof(vehicle));
            else if (GetFreePlaces() < 1)
                throw new InvalidOperationException(nameof(vehicle));

            _parking.Vehicles.Add(vehicle);

        }

        public void Dispose()
        {
            _logTimer.Stop();
            _logTimer.Dispose();
            _withdrawTimer.Stop();
            _withdrawTimer.Dispose();
            _parking.Reset();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count();
        }

        public TransactionInfo[] GetLastParkingTransactions() => TransactionInfos.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog() => _logService.Read();

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException(nameof(vehicle));
            else if (vehicle.Balance < 0)
                throw new InvalidOperationException(nameof(vehicle));

            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles.Where(v => v.Id == vehicleId).FirstOrDefault();

            if (vehicle == null)
                throw new ArgumentException(nameof(vehicle));

            vehicle.Balance += sum > 0
                ? sum 
                : throw new ArgumentException(nameof(sum));
        }
    }
}