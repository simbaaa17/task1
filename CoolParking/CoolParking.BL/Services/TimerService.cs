﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public Timer Timer { get; }
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            Interval = interval;
            Timer = new Timer(interval);
            Timer.Elapsed += OnElapsedHandler;
        }

        private void OnElapsedHandler(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, e);
        }

        public void Dispose()
        {
            Timer?.Dispose();
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }
    }
}