﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

public class LogService : ILogService
{
    public string LogPath { get; }

    public LogService(string logPath)
    {
        if (string.IsNullOrWhiteSpace(logPath))
            throw new ArgumentException("message", nameof(logPath));

        LogPath = logPath;
    }
    public string Read()
    {
        string result = string.Empty;

        try
        {
            using (var file = new StreamReader(LogPath))
            {
                result = file.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            throw new InvalidOperationException("File not found", e);
        }


        return result;
    }
    public void Write(string logInfo)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(logInfo);
            }
        }
        catch (FileNotFoundException e)
        {
            Console.WriteLine(e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
